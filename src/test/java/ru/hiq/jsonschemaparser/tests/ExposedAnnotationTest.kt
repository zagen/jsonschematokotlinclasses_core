
import org.junit.Test
import org.junit.Assert.*

import ru.hiq.jsonschemaparser.core.classes.Property
import ru.hiq.jsonschemaparser.core.annotation.ExposedAnnotation

internal class ExposedAnnotationTest {

    @Test
    fun test_EmptyFlagMap_SimpleExposeAnnotation() {
        val annotation = ExposedAnnotation()
        val property = Property("a", type = "", annotations = mutableListOf(annotation))
        assertEquals("@Expose", annotation.value(property))
    }

    @Test
    fun test_FlagsMapContainsValueWithBothTrue_SimpleExposeAnnotation() {
        val annotation = ExposedAnnotation(true, true)
        val property = Property("a", type = "", annotations = mutableListOf(annotation))
        assertEquals("@Expose", annotation.value(property))
    }

    @Test
    fun test_FlagsMapContainsValueSkipSerialize_ExposeAnnotationWithSerializeFalse() {
        val annotation = ExposedAnnotation(false, true)
        val property = Property("a", type = "", annotations = mutableListOf(annotation))
        assertEquals("@Expose(serialize = false)", annotation.value(property))
    }

    @Test
    fun test_FlagsMapContainsValueSkipDeserialize_ExposeAnnotationWithDeserializeFalse() {
        val annotation = ExposedAnnotation(true, false)
        val property = Property("a", type = "", annotations = mutableListOf(annotation))
        assertEquals("@Expose(deserialize = false)", annotation.value(property))
    }

    @Test
    fun test_FlagsMapContainsValueSkipSerializeDeserialize_ExposeAnnotationWithDeserializeFalse() {
        val annotation = ExposedAnnotation(false, false)
        val property = Property("a", type = "", annotations = mutableListOf(annotation))
        assertEquals("@Expose(serialize = false, deserialize = false)", annotation.value(property))
    }
}