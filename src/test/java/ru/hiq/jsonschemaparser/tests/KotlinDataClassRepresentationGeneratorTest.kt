package ru.hiq.jsonschemaparser.tests

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.doAnswer
import org.mockito.Mockito.doReturn
import org.mockito.MockitoAnnotations
import ru.hiq.jsonschemaparser.core.Settings
import ru.hiq.jsonschemaparser.core.classes.*

internal class KotlinDataClassRepresentationGeneratorTest {

    private val typePostfix = "1"

    @Mock
    lateinit var settings: Settings

    @Mock
    lateinit var formatter: TypeFormatter

    private lateinit var generator: ClassRepresentationGenerator

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        doReturn("ClassName").`when`(settings).rootClassName
        doAnswer { invocation -> invocation?.arguments?.first() as String + typePostfix }.`when`(formatter).format(Mockito.anyString())
        generator = KotlinDataClassRepresentationGenerator(settings, formatter)

    }

    @Test
    fun test_EmptyFileDescription_returnEmptyClass() {
        val description = ClassDescription("", emptyList())
        val representation = generator.generate(description)

        assertEquals("data class ClassName()", representation)
    }

    @Test
    fun test_EmptyNameFileDescriptionHasTwoPrimitiveParams_returnGeneratedClassRepresentation() {
        val description = ClassDescription("", listOf(Property("first", "Integer"), Property("second", "Boolean")))
        val representation = generator.generate(description)

        assertEquals("data class ClassName(val first:Integer$typePostfix, \nval second:Boolean$typePostfix)", representation)
    }

    @Test
    fun test_FileDescriptionHasOnePrimitiveOneObjectParams_returnGeneratedClassRepresentation() {
        val description = ClassDescription("OtherName", listOf(Property("first", "Double"), Property("second", "List<Integer>")))
        val representation = generator.generate(description)

        assertEquals("data class OtherName(val first:Double$typePostfix, \nval second:List<Integer>$typePostfix)", representation)
    }
}