import org.junit.Test
import org.junit.Assert.*
import org.junit.Before

import ru.hiq.jsonschemaparser.core.classes.JavaToKotlineTypeFormatter

internal class JavaToKotlineTypeFormatterTest {
    lateinit var formatter: JavaToKotlineTypeFormatter

    @Before
    fun setUp() {
        formatter = JavaToKotlineTypeFormatter()
    }

    @Test
    fun test_BooleanType_returnBooleanType() {
        assertEquals("Boolean", formatter.format("Boolean"))
    }

    @Test
    fun test_DoubleType_returnDoubleType() {
        assertEquals("Double", formatter.format("Double"))
    }

    @Test
    fun test_StringType_returnStringType() {
        assertEquals("Double", formatter.format("Double"))
    }

    @Test
    fun test_IntegerType_returnIntType() {
        assertEquals("Int", formatter.format("Integer"))
    }

    @Test
    fun test_IntegerListType_returnListIntType() {
        assertEquals("List<Int>", formatter.format("List<Integer>"))
    }

    @Test
    fun test_IntegerInnerListType_returnInnerListIntType() {
        assertEquals("List<List<Int>>", formatter.format("List<List<Integer>>"))
    }

    @Test
    fun test_ObjectType_returnObjectType() {
        assertEquals("SomeClassItem", formatter.format("SomeClassItem"))
    }

}