package ru.hiq.jsonschemaparser.tests

import org.junit.Test
import org.junit.Assert.*
import org.junit.Before

import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import ru.hiq.jsonschemaparser.core.JsonSchemaParser
import ru.hiq.jsonschemaparser.core.SchemaParser
import ru.hiq.jsonschemaparser.core.Settings
import ru.hiq.jsonschemaparser.core.classes.print


internal class JsonSchemaParserTest {
    @Mock
    lateinit var settings: Settings

    private lateinit var parser: SchemaParser
    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        doReturn("").`when`(settings).rootClassName
        parser = JsonSchemaParser(settings)
    }

    @Test
    fun test_EmptyJson_returnEmptyMap() {
        val json = "{\n" +
                "    }"
        val files = parser.parse(json)
        files.print();
        assertEquals(1, files.size)
        assertTrue(files.first().name.isEmpty())
        assertEquals(0, files.first().properties.size)
    }

    @Test
    fun test_JsonWithTwoStringValue_returnEmptyMapWithTwoProperty() {
        val json = "{\n" +
                "      \"customerId\": \"string\",\n" +
                "      \"customerName\": \"string\"\n" +
                "    }"
        val files = parser.parse(json)

        files.print();
        assertEquals(1, files.size)
        assertTrue(files.first().name.isEmpty())
        assertEquals(2, files.first().properties.size)
    }

    @Test
    fun test_JsonWithStringAndIntValue_returnEmptyMapWithTwoProperty() {
        val json = "{\n" +
                "      \"customerId\": \"string\",\n" +
                "      \"customerName\": 1.20 \n" +
                "    }"
        val files = parser.parse(json)

        files.print();
        assertEquals(1, files.size)
        assertTrue(files.first().name.isEmpty())
        assertEquals(2, files.first().properties.size)
        assertEquals("String", files.first().properties[0].type)
        assertEquals("Double", files.first().properties[1].type)
    }

    @Test
    fun test_JsonWithComplexValue_returnEmptyMapWithTwoProperty() {
        val json = "{\"otpAccepted\": true,\n" +
                "    \"otpInfo\": {\n" +
                "      \"currentOtpCard\": \"H0012345\",\n" +
                "      \"fixedOtpCard\": false,\n" +
                "      \"nextOtpCard\": \"H0098765\",\n" +
                "      \"nextOtpIndex\": \"string\"\n" +
                "    } }"
        val files = parser.parse(json)

        files.print();
        assertEquals(2, files.size)
        assertTrue(files.first().name.isEmpty())
        assertEquals(2, files.first().properties.size)
        assertEquals("Boolean", files.first().properties[0].type)
        assertEquals("OtpInfo", files.first().properties[1].type)

        //check second file
        assertEquals("OtpInfo", files[1].name)
        assertEquals(4, files[1].properties.size)
        assertEquals("String", files[1].properties[0].type)
        assertEquals("Boolean", files[1].properties[1].type)
        assertEquals("String", files[1].properties[2].type)
        assertEquals("String", files[1].properties[3].type)
    }

    @Test
    fun test_JsonWithPrimitiveArray_returnEmptyMapWithTwoProperty() {
        val json = "{\"otpAccepted\": true,\n" +
                "    \"otpInfo\": [1,2,3,4] }"
        val files = parser.parse(json)

        files.print();
        assertEquals(1, files.size)
        assertTrue(files.first().name.isEmpty())
        assertEquals(2, files.first().properties.size)
        assertEquals("Boolean", files.first().properties[0].type)
        assertEquals("List<Integer>", files.first().properties[1].type)

    }


    @Test
    fun test_JsonWithObjectArray_returnEmptyMapWithTwoProperty() {
        val json = "{\"otpAccepted\": true,\n" +
                "    \"otpInfo\": [{\"accountLocked\": true,\n" +
                "    \"customerName\": \"string\",},{\"accountLocked\": true,\n" +
                "    \"customerName\": \"string\",}] }"
        val files = parser.parse(json)

        files.print();
        assertEquals(2, files.size)
        assertTrue(files.first().name.isEmpty())
        assertEquals(2, files.first().properties.size)
        assertEquals("Boolean", files.first().properties[0].type)
        assertEquals("List<OtpInfoItem>", files.first().properties[1].type)


        //check second file
        assertEquals("OtpInfoItem", files[1].name)
        assertEquals(2, files[1].properties.size)
        assertEquals("Boolean", files[1].properties[0].type)
        assertEquals("String", files[1].properties[1].type)
    }


    @Test
    fun test_ComplexJsonWithObjectAndInnerArrays_returnMapWithThreeFiles() {
        val json = "{\"otpAccepted\": true,\n" +
                "    \"otpInfo\": [{\"accountLocked\": true,\n" +
                "    \"customerName\": {\n" +
                "    \"mustAcceptTerms\": true\n" +
                "  }}] }"
        val files = parser.parse(json)

        files.print();
        assertEquals(3, files.size)
        assertTrue(files.first().name.isEmpty())
        assertEquals(2, files.first().properties.size)
        assertEquals("Boolean", files.first().properties[0].type)
        assertEquals("List<OtpInfoItem>", files.first().properties[1].type)


        //check second file
        assertEquals("OtpInfoItem", files[1].name)
        assertEquals(2, files[1].properties.size)
        assertEquals("Boolean", files[1].properties[0].type)
        assertEquals("CustomerName", files[1].properties[1].type)

        //check third file
        assertEquals("CustomerName", files[2].name)
        assertEquals(1, files[2].properties.size)
        assertEquals("Boolean", files[2].properties[0].type)
    }

}