import org.junit.Test
import org.junit.Assert.*
import org.junit.Before

import ru.hiq.jsonschemaparser.core.classes.ClassDescription
import ru.hiq.jsonschemaparser.core.classes.ImportGenerator
import ru.hiq.jsonschemaparser.core.classes.ImportGeneratorImpl
import ru.hiq.jsonschemaparser.core.classes.Property
import ru.hiq.jsonschemaparser.core.annotation.ExposedAnnotation
import ru.hiq.jsonschemaparser.core.annotation.SerializedNameAnnotation

internal class ImportGeneratorTest {
    private lateinit var generator: ImportGenerator
    private lateinit var property: Property
    private lateinit var secondProperty: Property
    private lateinit var classDescriptionWithOneProperty: ClassDescription
    private lateinit var classDescriptionWithTwoProperties: ClassDescription
    @Before
    fun setUp() {
        generator = ImportGeneratorImpl()
        property = Property("a", "")
        secondProperty = Property("b", "")
        classDescriptionWithOneProperty = ClassDescription(properties = listOf(property))
        classDescriptionWithTwoProperties = ClassDescription(properties = listOf(property, secondProperty))

    }

    @Test
    fun test_EmptyClassDescription_importEmpty() {

        val result = generator.generate(ClassDescription())
        assertTrue(result.isEmpty())
    }

    @Test
    fun test_ClassDescriptionHasExposedAnnotation_importHasExposedValue() {
        property.annotations.add(ExposedAnnotation())
        val result = generator.generate(classDescriptionWithOneProperty)

        assertEquals(1, result.split("\n").size)
        assertTrue(result.contains("import com.google.gson.annotations.Expose"))

    }

    @Test
    fun test_ClassDescriptionHasSerializedAnnotation_importHasSerializedValue() {
        property.annotations.add(SerializedNameAnnotation())
        val result = generator.generate(classDescriptionWithOneProperty)

        assertEquals(1, result.split("\n").size)
        assertTrue(result.contains("import com.google.gson.annotations.SerializedName"))
    }

    @Test
    fun test_ClassDescriptionHasBothAnnotations_importHasBothValues() {
        property.annotations.add(SerializedNameAnnotation())
        property.annotations.add(ExposedAnnotation())
        val result = generator.generate(classDescriptionWithOneProperty)

        assertEquals(2, result.split("\n").size)
        assertTrue(result.contains("import com.google.gson.annotations.Expose"))
        assertTrue(result.contains("import com.google.gson.annotations.SerializedName"))
    }

    @Test
    fun test_ClassDescriptionWithTwoPropertiesHasBothAnnotations_importHasTwoValues() {
        property.annotations.add(SerializedNameAnnotation())
        property.annotations.add(ExposedAnnotation())

        secondProperty.annotations.add(SerializedNameAnnotation())
        secondProperty.annotations.add(ExposedAnnotation())
        val result = generator.generate(classDescriptionWithTwoProperties)

        assertEquals(2, result.split("\n").size)
        assertTrue(result.contains("import com.google.gson.annotations.Expose"))
        assertTrue(result.contains("import com.google.gson.annotations.SerializedName"))
    }

}