package ru.hiq.jsonschemaparser.core.classes


interface TypeFormatter {
    fun format(input: String): String
}