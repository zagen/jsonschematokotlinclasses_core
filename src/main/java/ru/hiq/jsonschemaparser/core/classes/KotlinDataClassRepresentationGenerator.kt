package ru.hiq.jsonschemaparser.core.classes

import ru.hiq.jsonschemaparser.core.Settings

class KotlinDataClassRepresentationGenerator(val settings: Settings,
                                             val formatter: TypeFormatter) : ClassRepresentationGenerator {
    override fun generate(classDescription: ClassDescription): String {
        var name = classDescription.name
        if (name.isEmpty()) {
            name = settings.rootClassName
        }
        return "data class $name(${generateArgs(classDescription.properties)})"
    }

    private fun generateArgs(properties: List<Property>): String {
        return properties
                .joinToString(separator = ", \n") { field(it) }
    }

    private fun field(property: Property): String {
        val sb = StringBuilder()
        sb.append(property.annotations.joinToString(separator = " ") { it.value(property) })
        sb.append("val ${property.targetName}:${formatter.format(property.type)}")
        return sb.toString()
    }
}