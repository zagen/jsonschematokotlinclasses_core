package ru.hiq.jsonschemaparser.core.classes

interface ClassRepresentationGenerator {
    fun generate(classDescription: ClassDescription): String
}
