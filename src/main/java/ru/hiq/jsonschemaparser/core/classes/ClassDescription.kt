package ru.hiq.jsonschemaparser.core.classes

class ClassDescription(var name: String = "", val properties: List<Property> = emptyList()) {
    override fun toString(): String = "$name ${properties.joinToString(", ", prefix = "(", postfix = ")") { property -> "${property.targetName} : ${property.type}" }}"
}

fun List<ClassDescription>.print() = print(this.toString() + "\n")

