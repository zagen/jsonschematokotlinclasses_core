package ru.hiq.jsonschemaparser.core.classes

class JavaToKotlineTypeFormatter : TypeFormatter {
    private val mapping = mapOf("<Integer>" to "<Int>", "Integer" to "Int")

    override fun format(input: String): String {
        mapping.forEach {
            if (input.contains(it.key)) {
                return input.replace(it.key, it.value)
            }
        }
        return input
    }
}