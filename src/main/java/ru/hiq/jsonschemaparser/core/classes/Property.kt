package ru.hiq.jsonschemaparser.core.classes

import ru.hiq.jsonschemaparser.core.annotation.Annotation

class Property(val sourceName: String,
               val type: String,
               var targetName: String = sourceName,
               val annotations: MutableList<Annotation> = mutableListOf()) {
    fun component1(): String = sourceName
    fun component2(): String = type
    fun component3(): String = targetName
}