package ru.hiq.jsonschemaparser.core.classes

interface ImportGenerator {
    fun generate(classDescription: ClassDescription): String
}