package ru.hiq.jsonschemaparser.core.classes

import ru.hiq.jsonschemaparser.core.annotation.ExposedAnnotation
import ru.hiq.jsonschemaparser.core.annotation.SerializedNameAnnotation
import ru.hiq.jsonschemaparser.core.utils.default

class ImportGeneratorImpl : ImportGenerator {
    val map: Map<String, String> = mapOf(SerializedNameAnnotation::class.java.simpleName!! to "import com.google.gson.annotations.SerializedName",
            ExposedAnnotation::class.java.simpleName!! to "import com.google.gson.annotations.Expose")

    override fun generate(classDescription: ClassDescription): String = classDescription.properties
            .flatMap { it.annotations }
            .distinctBy { it::class.java.simpleName }
            .joinToString(separator = "\n") {
                val key: String = it::class.java.simpleName ?: String.default()
                if (map.containsKey(key))
                    map.get(key) ?: String.default()
                else
                    String.default()
            }
}