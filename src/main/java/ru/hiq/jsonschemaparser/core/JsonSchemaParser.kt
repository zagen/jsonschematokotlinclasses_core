package ru.hiq.jsonschemaparser.core

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import ru.hiq.jsonschemaparser.core.classes.ClassDescription
import ru.hiq.jsonschemaparser.core.classes.Property
import java.text.ParseException


class JsonSchemaParser(val settings: Settings) : SchemaParser {
    private val _arrayItemTypePostfix = "Item"
    private val _parser: Parser = Parser()

    override fun parse(json: String): List<ClassDescription> {
        val files = mutableListOf<ClassDescription>()
        try {
            val jsonObject = _parser.parse(StringBuilder(json)) as JsonObject
            val queue = mutableMapOf(settings.rootClassName to jsonObject)
            while (queue.isNotEmpty()) {
                val (name, value) = queue.iterator().next()
                queue.remove(name)
                files.add(ClassDescription(name, value.map.map { entry ->
                    Property(entry.key, type(queue, entry.key, entry.value))
                }))
            }
        } catch (exception: Exception) {
            throw ParseException("", 0)
        }

        return files
    }

    private fun type(queue: MutableMap<String, JsonObject>, key: String, value: Any?): String =
            when (value) {
                is JsonObject -> {
                    val name = key.capitalize()
                    queue.put(name, value)
                    name
                }
                is JsonArray<*> -> {
                    val type = arrayItemType(queue, key, value)
                    formatArrayType(type)

                }
                else -> value!!::class.java.simpleName
            }

    private fun arrayItemType(queue: MutableMap<String, JsonObject>, key: String, value: JsonArray<*>): String =
            when {
                value.isEmpty() -> ""
                else -> {
                    val item = value.first()
                    type(queue, key + _arrayItemTypePostfix, item)
                }
            }

    private fun formatArrayType(type: String): String =
            when {
                type.isEmpty() -> "List"
                else -> "List<$type>"
            }
}