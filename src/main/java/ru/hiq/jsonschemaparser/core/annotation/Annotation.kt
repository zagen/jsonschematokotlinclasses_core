package ru.hiq.jsonschemaparser.core.annotation

import ru.hiq.jsonschemaparser.core.classes.Property

interface Annotation {
    fun value(property: Property): String
}