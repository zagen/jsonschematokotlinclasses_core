package ru.hiq.jsonschemaparser.core.annotation

import ru.hiq.jsonschemaparser.core.classes.Property

class SerializedNameAnnotation : Annotation {
    override fun value(property: Property) = "@SerializedName(\"${property.sourceName}\")"
}

class ExposedAnnotation(val serialized: Boolean = true, val deserialize: Boolean = true) : Annotation {
    override fun value(property: Property) = "@Expose${buildAnnotationValue(serialized, deserialize)}"


    private fun buildAnnotationValue(serialized: Boolean, deserialize: Boolean): String {
        if (serialized && deserialize) {
            return ""
        }

        val annotationValueBuilder = StringBuilder()
        annotationValueBuilder.append("(")
        if (!serialized) {
            annotationValueBuilder.append("serialize = false")
        }
        if (!deserialize) {
            if (annotationValueBuilder.length > 1) annotationValueBuilder.append(", ")
            annotationValueBuilder.append("deserialize = false")
        }
        annotationValueBuilder.append(")")
        return annotationValueBuilder.toString()
    }
}