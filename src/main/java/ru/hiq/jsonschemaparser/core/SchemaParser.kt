package ru.hiq.jsonschemaparser.core

import ru.hiq.jsonschemaparser.core.classes.ClassDescription
import java.text.ParseException

interface SchemaParser {
    @Throws(ParseException::class)
    fun parse(json: String): List<ClassDescription>
}