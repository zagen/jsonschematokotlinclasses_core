package ru.hiq.jsonschemaparser.core.files

import ru.hiq.jsonschemaparser.core.classes.ClassDescription

interface FilesGenerator {
    fun generate(classDescriptions: List<ClassDescription>): List<File>
}