package ru.hiq.jsonschemaparser.core.files

import ru.hiq.jsonschemaparser.core.Settings
import ru.hiq.jsonschemaparser.core.classes.ClassDescription
import ru.hiq.jsonschemaparser.core.classes.ClassRepresentationGenerator
import ru.hiq.jsonschemaparser.core.classes.ImportGenerator

class MultipleKotlinDataClassFilesGenerator(private val settings: Settings,
                                            private val importGenerator: ImportGenerator,
                                            private val classRepresentationGenerator: ClassRepresentationGenerator) : FilesGenerator {
    override fun generate(classDescriptions: List<ClassDescription>): List<File> =
            classDescriptions.map {
                File("${it.name}.kt",
                        "package ${settings.packageName}",
                        importGenerator.generate(it),
                        classRepresentationGenerator.generate(it))
            }


}