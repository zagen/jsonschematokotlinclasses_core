package ru.hiq.jsonschemaparser.core.files

class File(
        val name: String,
        val packageValue: String,
        val imports: String,
        val content: String) {


    fun value() = "$packageValue\n\n$imports\n\n$content"
}