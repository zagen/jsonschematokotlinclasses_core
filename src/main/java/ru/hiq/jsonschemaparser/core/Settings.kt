package ru.hiq.jsonschemaparser.core

interface Settings {
    var rootClassName: String
    var packageName: String
}

class SettingsRepo(override var rootClassName: String, override var packageName: String) : Settings

